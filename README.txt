Search API Elasticsearch Synonym

This module provides synonym functionality for Elasticsearch powered by Search API.

Go to /admin/config/search/search-api-elasticsearch-synonym to configure this module.
